
# Use the following command in your terminal to create the Terraform and GitLab-CI environments/jobs
## make terra-ci ENVS="development staging production"
# Note that the environment names themselves must be seperated my spaces.

terra-ci:
	@curl -o terra-ci.py "https://gitlab.com/bruno-salvatico/terra-ci/-/raw/main/terra-ci.py" 
	@python3 terra-ci.py envs $(ENVS)
	@rm terra-ci.py
