# Terra-CI
Terra-CI simply creates a multi environment file structure and .gitlab-ci.yml file based on the environments you would like to deploy to from a single repo to work with GitLabs CI/CD and the built-in Terraform backend state feature.
## Features
- Input environments as arguements using make or python such as "development, staging, production"
- Creates .gitlab-ci.yml with validate, plan, apply, destroy jobs ✨
- Creates terraform/<ENV> subdirectories based on input with AWS provider.tf and backend.tf ✨
## Requirements
| Tool | Required | Notes |
| ------ | ------ | ------ |
| Make | Optional but recommended | The "make" tool may not come by default on some Windows, OS X, and Linux systems. |
| Python3 | Required | The examples below use python3 which make or may not be set in your systems as a python alias. |

## How to Use
### Make (Recommended):

Create a "Makefile" at the root of your GitLab repository/project with the following contents or copy from [here](https://gitlab.com/bruno-salvatico/terra-ci/-/blob/main/Makefile)
```
terra-ci:
	@curl -o terra-ci.py "https://gitlab.com/bruno-salvatico/terra-ci/-/raw/main/terra-ci.py" 
	@python3 terra-ci.py envs $(ENVS)
	@rm terra-ci.py
```
Run the following command from this directory with the environment names seperated by spaces
```bash
make terra-ci ENVS="development staging production"
```
**Note:** 
- *This method less error prone and allows you to get the latest version of the terra-ci.py script every time you run it as new tweaks and features are added.*

### Python:
Download the [terra-ci.py](https://gitlab.com/bruno-salvatico/terra-ci/-/blob/main/terra-ci.py) script to the root of your GitLab repository/project and apply the following command follwed by the environments seperated by spaces.
```python
python3 terra_ci.py envs development staging production
```
**Notes:** 
 - *The argument `envs` followed by one or more environments seperated by spaces.*
