import os
import textwrap
import sys

# Add the environments (AWS accounts) you would like to deploy to. 
# Logic such as only allowing production to deploy on a protected branch has been applied at the ci-template level
# For a full list of a counts, reference the README.md

# Examaple: envs ["development","staging","production"]
args = sys.argv

class EnvironmentSetup:
    def __init__(self, args):
        print ("Setting up project/repo from environment list")
        self.args = args
        self.envs = set()
        self.action = None
        try:
          if self.args[1] != "envs":
              print ("Your argument does not match one of the acceptable agurments\nDid you mean 'envs'?")
              return
          elif len(self.args) < 3:
              print ('Your environments are empty, please try again using the following format\nmake terra-ci ENVS="env1 env2 env3"')
          elif self.args[1] == "envs":
                  setOfEnvironments = set(self.args[2:])   
                  for env in setOfEnvironments:
                      self.envs.add(str.lower(env))  
        except Exception as e:
          print ("Something went wrong when trying to convert your environments to a set\n",e)
          return
        self.create_ci()
        self.create_tf()

    def create_ci (self):
#        for loop here
        if os.path.exists (".gitlab-ci.yml"):
            with open(".gitlab-ci.yml", "r") as ci:
                content = ci.read()
                for env in self.envs:
                    if f"name: {str(env)}" not in content:
                        self.env = env
                        self.action = "a"
                        self.create_jobs()
                    else:
                        pass
        else:
            self.create_stages()
            for env in self.envs:
                self.env = env
                self.create_jobs()

    def create_stages (self):
        try:
          print ("Creating gitlab-ci.yml stages Terraform CI/CD Pipeline")
          with open(".gitlab-ci.yml", "w") as include:
              include.write(textwrap.dedent('''\
              ### Generated by setup.py
              include: 
                - project: 'themotleyfool/infrastructure/terraform/ci-templates/multi-env-project'
                  file: 'terraform-project.gitlab-ci.yml'
                  ref: main
              stages:
                - validate
                - plan
                - apply
              #  - destroy
              \n'''))
              include.close()
              self.action = "a"
        except Exception as e: 
          print("An error occured when trying to create stages gitlab-ci.yml file\n",e) 
    def create_jobs (self):
        try:
            print ("Creating gitlab-ci.yml jobs Terraform CI/CD Pipeline")
            e = ' '.join(self.envs)
            print (f"Creating jobs for {e}")
            if "-" in self.env:
              print (f"Dashes in {self.env} will be replaced with underscores to follow GitLab ENV variable conventions.")
              self.env = self.env.replace("-","_")
            with open(".gitlab-ci.yml", self.action) as jobs:
                jobs.write(textwrap.dedent(f'''\
                # Jobs for {self.env} environment
                validate:{self.env}:
                  extends: .terraform:validate
                  environment: 
                    name: {self.env}
                plan:{self.env}:
                  extends: .terraform:plan
                  environment: 
                    name: {self.env}
                  needs: ["validate:{self.env}"]
                apply:{self.env}:
                  extends: .terraform:apply
                  environment: 
                    name: {self.env}
                    url: https://<PROJECT_NAME>{self.env}.fool.com
                  needs: ["plan:{self.env}"]
                # destroy:{self.env}:
                #   extends: .terraform:destroy
                #   environment: 
                #     name: {self.env}
                #   needs: ["apply:{self.env}"]\n
                '''))
                jobs.close()
        except Exception as e: 
          print("An error occured when trying to create the jobs for gitlab-ci.yml file\n", e)            
    def create_tf (self):
        e = ' '.join(self.envs)
        print (f"Creating Terraform working directory for {e}")
        try:
          for env in self.envs:
              env = env.replace("-","_")
              env_path = f"terraform/{env}"
              isExists = os.path.exists(env_path)
              if not isExists:
                  # Create dir with each environment name on the list
                    os.makedirs(env_path)
                  # Create provider the points to the corresponding AWS Account based on env name
                    path = os.path.join(env_path,"providers.tf")
                    with open(path,"w") as tf:
                        tf.write(textwrap.dedent('''\
                        ### Generated by setup.py
                        provider "aws" {
                          assume_role {
                            role_arn     = "arn:aws:iam::${data.terraform_remote_state.core.outputs.account_id}:role/gitlab-runner-service-role"
                            session_name = "gitlab-ci_crossaccount_terraform"
                          }
                          region = "us-east-1"
                        }
                         '''))
                        tf.close()
                  #     Create the backend that Terraform will use to store it's state file in GitLab
                    path = os.path.join(env_path,"backend.tf")
                    with open(path,"w") as backend:
                        backend.write(textwrap.dedent('''\
                        ### Generated by setup.py
                        # Backend that Terraform will use to store it's state file in GitLab
                        terraform {
                          backend "http" {
                          }
                        }
                        variable "remote_state" {
                          description = "Calls remote state for this environment from the core infrastructure. Enables calling of vpc_id, account_id, and other resources using TF_VAR declared in GitLab-CI tempalte"
                            type        = string
                        }
                        data "terraform_remote_state" "core" {
                          backend = "http"
                          config = {
                            address = var.remote_state
                          }
                        }
                         '''))
                        backend.close()
        except: 
          print ("An error occured when trying to create your Terraform directory and files")




if __name__ == "__main__":
    start = EnvironmentSetup(args)

